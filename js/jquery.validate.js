function validate(formId){
	var status = true;
	try{
		jQuery('#' + formId + ' input[type=text], input[type=password]').each(function(i, e){
			var elemento = jQuery(e).val();
			if(elemento == ''){
				throw('Por favor, preencha todos os campos deste formulário.');
			}
		});
	}catch(e){
		alert(e);
		status = false;
	}
	return status;
}