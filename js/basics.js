
var sizeDefault = 12;
function aumentaTexto(){
    sizeDefault += 5;
    jQuery('.page__content_wrapper_content *').css({'font-size':sizeDefault+'px'});
}
function diminuiTexto(){
    sizeDefault -= 5;
    jQuery('.page__content_wrapper_content *').css({'font-size':sizeDefault+'px'});
}

function mostrarArtigo(){
    var dsAncora = "a"+jQuery('#nr_artigo').val()+"";
    jQuery("a[name='"+dsAncora+"']").attr('id',dsAncora);
    window.location.hash = '#'+dsAncora;
}

var flTextoTachado = true;
function textoTachado(){
    if(flTextoTachado){
        jQuery("#conteudo_diploma s, #conteudo_diploma strike").css("text-decoration", "none");
        flTextoTachado = false;
    }else{
        jQuery("#conteudo_diploma s, #conteudo_diploma strike").css("text-decoration", "line-through");
        flTextoTachado = true;
    }
}

jQuery(document).ready(function(){

    jQuery(".page__content_box_header_icon.right").live('click', function(event){
        var currentIcone = jQuery(event.target).attr('src');
        
        if(currentIcone.indexOf('img/bloco-fechar.png') != -1){
            jQuery(event.target).attr('src', 'img/bloco-abrir.png')
                .attr('title', 'Abrir');
            jQuery(event.target)
                .parents('div.page__content_box')
                .find('div.page__content_box_content').hide(250);

        }else if(currentIcone.indexOf('img/bloco-abrir.png') != -1){
            jQuery(event.target).attr('src', 'img/bloco-fechar.png')
                .attr('title', 'Fechar');
            jQuery(event.target)
                .parents('div.page__content_box')
                .find('div.page__content_box_content').show(250);
        }
    });
    
});